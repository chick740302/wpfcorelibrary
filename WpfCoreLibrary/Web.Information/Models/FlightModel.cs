﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Information.Models
{
    public class FlightModel
    {
        public string Termernal { get; set; }
        public string Immigration { get; set; } //Departure, Arrival
        public string AirlineCode { get; set; }
        public string AirlineChtName { get; set; }
        public string FlightNumber { get; set; }
        public string Gate { get; set; }
        public string ScheduledDate { get; set; }
        public string ScheduledTime { get; set; }
        public string ExpectedDate { get; set; }
        public string ExpectedTime { get; set; }
        public string OriginIATA { get; set; }
        public string OriginEng { get; set; }
        public string OriginCht { get; set; }
        public string FlightStatus { get; set; }
        public string PlaneType { get; set; }
        public string ViaIATA { get; set; }
        public string ViaEng { get; set; }
        public string ViaCht { get; set; }
        public string BaggageCarousel { get; set; }
        public string CheckInCounter { get; set; }
    }
}
