﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using static Web.Information.Models.EarthquakeModel;

namespace Web.Information.Retriever
{
    public class EarthquakeInfoModel
    {
        public string Place { get; set; }
        public DateTime Time { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Depth { get; set; }
        public double? MagnitudeScale { get; set; }
        public string Note { get; set; }
    }

    public class Earthquake : BaseRetriever
    {
        public List<EarthquakeInfoModel> model;

        private readonly string EARTH_QUAKE_API_URL = "https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime={0}&endtime={1}&minmagnitude={2}";
        private const int MIN_EARTH_QUAKE = 3;

        protected override void Dispose()
        {
            ;
        }

        public List<EarthquakeInfoModel> GetRecentEarthquake()
        {
            object obj = GetData();
            return obj == null ? null : (List<EarthquakeInfoModel>)obj;
        }

        public async Task<List<EarthquakeInfoModel>> GetRecentEarthquakeAsync()
        {
            object obj = null;
            await Task.Run(() => 
            {
                obj = GetData();
            }); 
            return obj == null ? null : (List<EarthquakeInfoModel>)obj;
        }

        protected override object GetData()
        {
            List<EarthquakeInfoModel> earthquake = new List<EarthquakeInfoModel>();
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    var end = DateTime.Now.ToString("yyyy-MM-dd");
                    var start = DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd");
                    string url = string.Format(EARTH_QUAKE_API_URL, start, end, MIN_EARTH_QUAKE);
                    var result = webClient.DownloadString(url);
                    var model = JsonConvert.DeserializeObject<RootObject>(result);
                    if (model != null && model.features != null)
                    {
                        var initialDate = new DateTime(1970, 1, 1);
                        foreach (var feature in model.features)
                        {
                            var m = new EarthquakeInfoModel()
                            {
                                Place = feature.properties.place,
                                Time = initialDate.AddMilliseconds(feature.properties.time.Value),
                                Longitude = feature.geometry.coordinates[0],
                                Latitude = feature.geometry.coordinates[1],
                                Depth = feature.geometry.coordinates[2],
                                MagnitudeScale = feature.properties.mag
                            };
                            earthquake.Add(m);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            return earthquake;
        }
    }
}
