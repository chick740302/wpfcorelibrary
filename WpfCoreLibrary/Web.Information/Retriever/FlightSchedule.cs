﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Web.Information.Models;

namespace Web.Information.Retriever
{
    public class FlightSchedule : BaseRetriever
    {
        private const string FLIGHT_SCHEDULE_URL = "https://www.taoyuan-airport.com/uploads/flightx/a_flight_v4.txt";
        private Timer timer;

        public FlightSchedule(bool autoRefresh = false, int pollingSeconds = 60)
        {
            if (autoRefresh)
            {
                timer = new Timer();
                timer.Interval = pollingSeconds * 1000;
                timer.Elapsed += Timer_Elapsed;
                timer.Start();
            }
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ;
        }

        protected override object GetData()
        {
            var flightStr = string.Empty;
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.Encoding = Encoding.Default;
                    flightStr = webClient.DownloadString(FLIGHT_SCHEDULE_URL);
                }

                var items = flightStr.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                var flightModels = new List<FlightModel>();
                foreach (var item in items)
                {
                    var result = item.Split(new string[] { "," }, StringSplitOptions.None);
                    FlightModel model = new FlightModel()
                    {
                        Termernal = result[0].Trim(),
                        Immigration = result[1].Trim(),
                        AirlineCode = result[2].Trim(),
                        AirlineChtName = result[3].Trim(),
                        FlightNumber = result[4].Trim(),
                        Gate = result[5].Trim(),
                        ScheduledDate = result[6].Trim(),
                        ScheduledTime = result[7].Trim(),
                        ExpectedDate = result[8].Trim(),
                        ExpectedTime = result[9].Trim(),
                        OriginIATA = result[10].Trim(),
                        OriginEng = result[11].Trim(),
                        OriginCht = result[12].Trim(),
                        FlightStatus = result[13].Trim(),
                        PlaneType = result[14].Trim(),
                        ViaIATA = result[15].Trim(),
                        ViaEng = result[16].Trim(),
                        ViaCht = result[17].Trim(),
                        BaggageCarousel = result[18].Trim(),
                        CheckInCounter = result[19].Trim()
                    };
                    flightModels.Add(model);
                }
                var dateNow = DateTime.Now;
                var dateDest = DateTime.Now.AddMinutes(45);
                var test = from a in flightModels
                           let d1 = DateTime.Parse(a.ExpectedDate + " " + a.ExpectedTime)
                           let d2 = DateTime.Parse(a.ScheduledDate + " " + a.ScheduledTime)
                           where (d1 >= dateNow && d1 <= dateDest) || (d2 >= dateNow && d2 <= dateDest)
                           select a;
                return test.ToList();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            return null;
        }

        protected override void Dispose()
        {
            if (timer != null)
            {
                timer.Stop();
                timer.Elapsed -= Timer_Elapsed;
            }
        }

        public List<FlightModel> GetFlightSchedule()
        {
            return (List<FlightModel>)GetData();
        }

        public async Task<List<FlightModel>> GetFlightScheduleAsync()
        {
            object obj = null;
            await Task.Run(() => 
            {
                obj = GetData();
            });
            return obj == null ? null : (List<FlightModel>)obj;
        }
    }
}
