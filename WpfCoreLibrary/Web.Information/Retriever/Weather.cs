﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using static Web.Information.Models.WeatherModel;

namespace Web.Information.Retriever
{
    public class Weather : BaseRetriever
    {
        private const string API_KEY = "d0ece83ead11fb798679b94ed543ff95";
        private const string WEATHER_URL = "http://api.openweathermap.org/data/2.5/group?id={0}&units=metric&appid={1}";

        private List<string> cityIds;

        public List<string> CityIds { get => cityIds; set => cityIds = value; }

        protected override void Dispose()
        {
            ;
        }

        protected override object GetData()
        {
            const int MAX = 20;
            int currentCount = 0;
            var weatherList = new List<List>();
            try
            {
                while (currentCount < cityIds.Count)
                {
                    var temp = (from a in cityIds select a).Skip(currentCount).Take(MAX).ToArray();
                    var ids = string.Join(",", temp);
                    var weatherUrl = string.Format(WEATHER_URL, ids, API_KEY);
                    using (WebClient webClient = new WebClient())
                    {
                        var json = webClient.DownloadString(weatherUrl);
                        var obj = JsonConvert.DeserializeObject<RootObject>(json);
                        weatherList.AddRange(obj.list);
                    }
                    currentCount += MAX;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            return weatherList;
        }

        public List<List> GetWeatherFromCity(List<string> models)
        {
            CityIds = models;
            return (List<List>)GetData();
        }

        public async Task<List<List>> GetWeatherFromCityAsync(List<string> models)
        {
            CityIds = models;
            object obj = null;
            await Task.Run(() => 
            {
                obj = GetData();
            });
            return obj == null ? null : (List<List>)obj;
        }
    }
}
