﻿using System.Threading.Tasks;

namespace Web.Information.Retriever
{
    public abstract class BaseRetriever
    {
        protected abstract object GetData();

        protected abstract void Dispose();
    }
}
