﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Wpf.UI.Core.Controls.Base
{
    public class BaseRadioButton : RadioButton
    {
        public int IntegerTag { get; set; }

        public BaseRadioButton()
        {
            StylusSystemGesture += BaseRadioButton_StylusSystemGesture;
        }

        private void BaseRadioButton_StylusSystemGesture(object sender, StylusSystemGestureEventArgs e)
        {
            if (e.SystemGesture == SystemGesture.Tap)
            {
                base.OnClick();
            }
        }

        #region IdleBrush
        public Brush IdleBrush
        {
            get { return (Brush)GetValue(IdleBrushProperty); }
            set { SetValue(IdleBrushProperty, value); }
        }

        public static DependencyProperty IdleBrushProperty =
            DependencyProperty.Register(
            "IdleBrush",
            typeof(Brush),
            typeof(BaseRadioButton),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(IdleBrushPropertyChanged)));

        private static void IdleBrushPropertyChanged(DependencyObject src, DependencyPropertyChangedEventArgs e)
        {
            ;
        }

        #endregion

        #region ActiveBrush

        public Brush ActiveBrush
        {
            get { return (Brush)GetValue(ActiveBrushProperty); }
            set { SetValue(ActiveBrushProperty, value); }
        }

        public static DependencyProperty ActiveBrushProperty =
            DependencyProperty.Register(
            "ActiveBrush",
            typeof(Brush),
            typeof(BaseRadioButton),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(ActiveBrushPropertyChanged)));

        private static void ActiveBrushPropertyChanged(DependencyObject src, DependencyPropertyChangedEventArgs e)
        {
            ;
        }

        #endregion
    }
}
