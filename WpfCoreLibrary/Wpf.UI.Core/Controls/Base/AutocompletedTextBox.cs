﻿using System;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;

namespace Wpf.UI.Core.Controls.Base
{
    public class AutocompletedTextBox : TextBox
    {
        private string[] autocompleteMailDomain = { "gmail.com", "hotmail.com", "outlook.com", "yahoo.com", "163.com", "qq.com" };
        private char triggerDetectChar = '@';

        public bool IsAutocompletedEnable = true;

        public string[] AutocompleteMailDomain
        {
            get { return autocompleteMailDomain; }
            set { autocompleteMailDomain = value; }
        }

        public char TriggerDetectChar
        {
            get { return triggerDetectChar; }
            set { triggerDetectChar = value; }
        }

        public AutocompletedTextBox()
        {
            PreviewKeyDown += AutocompletedTextBox_PreviewKeyDown;
            TextChanged += AutocompletedTextBox_TextChanged;
        }

        private void AutocompletedTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsAutocompletedEnable)
            {
                if (Text.Contains(triggerDetectChar))
                {
                    string[] splitResult = Text.Split(triggerDetectChar);
                    if (splitResult.Length > 1)
                    {
                        string suggest = GetSuggestMailDomain(splitResult[splitResult.Length - 1]);
                        if (!String.IsNullOrEmpty(suggest))
                        {
                            Text += suggest;
                            Select(Text.Length - suggest.Length, suggest.Length);
                            Focus();
                        }
                        else
                        {
                            if (CaretIndex == this.Text.Length)
                            {
                                var rect = GetRectFromCharacterIndex(CaretIndex);
                                ScrollToHorizontalOffset(rect.Right);
                            }
                        }
                    }
                }
            }
        }

        private void AutocompletedTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete || e.Key == Key.Back)
            {
                IsAutocompletedEnable = false;
            }
            else
            {
                IsAutocompletedEnable = true;
            }
        }

        private string GetSuggestMailDomain(string currentInput)
        {
            string result = String.Empty;
            if (String.IsNullOrEmpty(currentInput))
            {
                return result;
            }
            foreach (string domain in autocompleteMailDomain)
            {
                if (domain.StartsWith(currentInput))
                {
                    result = domain.Substring(currentInput.Length, domain.Length - currentInput.Length);
                }
            }
            return result;
        }
    }
}
