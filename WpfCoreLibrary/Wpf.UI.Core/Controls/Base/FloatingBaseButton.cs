﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Wpf.UI.Core.Animations;

namespace Wpf.UI.Core.Controls.Base
{
    public class FloatingBaseButton : BaseButton
    {
        #region IsFloating
        public bool IsFloating
        {
            get { return (bool)GetValue(IsFloatingProperty); }
            set { SetValue(IsFloatingProperty, value); }
        }

        public static DependencyProperty IsFloatingProperty =
            DependencyProperty.Register(
            "IsFloating",
            typeof(bool),
            typeof(FloatingBaseButton),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(IsFloatingPropertyChanged)));

        private static void IsFloatingPropertyChanged(DependencyObject src, DependencyPropertyChangedEventArgs e)
        {
            var btn = src as FloatingBaseButton;
            var floating = (bool)e.NewValue;
            if (btn != null)
            {
                if (floating)
                {
                    btn.StartFloating();
                }
                else
                {
                    btn.StopFloating();
                }
            }
        }

        #endregion

        private Random random = new Random(Guid.NewGuid().GetHashCode());
        private LinearMatrixAnimation linearAnimation;
        private Matrix targetMatrix;
        private const int MIN_DURATION_SECONDS = 3;
        private const int MAX_DURATION_SECONDS = 8;
        private const int MIN_X_VALUE = -5;
        private const int MAX_X_VALUE = 5;
        private const int MIN_Y_VALUE = -50;
        private const int MAX_Y_VALUE = 50;

        public FloatingBaseButton()
        {
            RenderTransform = new MatrixTransform(1, 0, 0, 1, 0, 0);
            RenderTransformOrigin = new Point(0.5, 0.5);
        }

        private void StartFloating()
        {
            if (!RenderTransform.GetType().Equals(typeof(MatrixTransform)))
            {
                RenderTransform = new MatrixTransform(1, 0, 0, 1, 0, 0);
                RenderTransformOrigin = new Point(0.5, 0.5);
            }
            var oldMatrix = (RenderTransform as MatrixTransform).Matrix;
            var duration = random.Next(MIN_DURATION_SECONDS, MAX_DURATION_SECONDS) + random.NextDouble();
            var x = random.Next(MIN_X_VALUE, MAX_X_VALUE);
            var y = random.Next(MIN_Y_VALUE, MAX_Y_VALUE);
            y = (oldMatrix.OffsetY > 0 && y > 0) || (oldMatrix.OffsetY < 0 && y < 0) ? -y : y;
            y = Math.Abs(oldMatrix.OffsetY - y) > 10 ? y : y * 2;
            targetMatrix = new Matrix(oldMatrix.M11, oldMatrix.M12, oldMatrix.M21, oldMatrix.M22, x, y);
            linearAnimation = new LinearMatrixAnimation(oldMatrix, targetMatrix, new Duration(TimeSpan.FromSeconds(duration)));
            //linearAnimation.EasingFunction = new ExponentialEase() { EasingMode = EasingMode.EaseIn };
            linearAnimation.FillBehavior = FillBehavior.HoldEnd;
            linearAnimation.Completed += LinearAnimation_Completed;
            RenderTransform.BeginAnimation(MatrixTransform.MatrixProperty, linearAnimation, HandoffBehavior.SnapshotAndReplace);
        }

        private void LinearAnimation_Completed(object sender, EventArgs e)
        {
            linearAnimation.Completed -= LinearAnimation_Completed;
            BeginAnimation(MatrixTransform.MatrixProperty, null);
            RenderTransform = new MatrixTransform(targetMatrix);
            StartFloating();
        }

        private void StopFloating()
        {
            linearAnimation.Completed -= LinearAnimation_Completed;
            BeginAnimation(MatrixTransform.MatrixProperty, null);
            RenderTransform = new MatrixTransform(1, 0, 0, 1, 0, 0);
        }
    }
}
