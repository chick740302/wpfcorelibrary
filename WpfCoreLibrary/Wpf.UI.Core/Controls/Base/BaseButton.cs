﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Wpf.UI.Core.Controls.Base
{
    public class BaseButton : Button
    {
        private bool isGestureEnabled = true;

        public int IntegerTag { get; set; }

        public bool IsGestureEnabled { get => isGestureEnabled; set => isGestureEnabled = value; }

        public BaseButton()
        {
            StylusSystemGesture += BaseButton_StylusSystemGesture;
        }

        private void BaseButton_StylusSystemGesture(object sender, StylusSystemGestureEventArgs e)
        {
            if (e.SystemGesture == SystemGesture.Tap && IsGestureEnabled)
            {
                base.OnClick();
            }
        }

        #region IdleBrush
        public Brush IdleBrush
        {
            get { return (Brush)GetValue(IdleBrushProperty); }
            set { SetValue(IdleBrushProperty, value); }
        }

        public static DependencyProperty IdleBrushProperty =
            DependencyProperty.Register(
            "IdleBrush",
            typeof(Brush),
            typeof(BaseButton),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(IdleBrushPropertyChanged)));

        private static void IdleBrushPropertyChanged(DependencyObject src, DependencyPropertyChangedEventArgs e)
        {
            ;
        }

        #endregion

        #region ActiveBrush

        public Brush ActiveBrush
        {
            get { return (Brush)GetValue(ActiveBrushProperty); }
            set { SetValue(ActiveBrushProperty, value); }
        }

        public static DependencyProperty ActiveBrushProperty =
            DependencyProperty.Register(
            "ActiveBrush",
            typeof(Brush),
            typeof(BaseButton),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(ActiveBrushPropertyChanged)));

        private static void ActiveBrushPropertyChanged(DependencyObject src, DependencyPropertyChangedEventArgs e)
        {
            ;
        }

        #endregion
    }
}
