﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Wpf.UI.Core.Controls.Base
{
    public abstract class BasePage : UserControl
    {
        //public delegate void PageClosing(object obj);
        //public event PageClosing OnPageClosing;

        public BasePage()
        {
            Style = new Style(GetType(), FindResource(typeof(BasePage)) as Style);
            Unloaded += BasePage_Unloaded;
        }

        private void BasePage_Unloaded(object sender, RoutedEventArgs e)
        {
            Dispose();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
        }

        public void ClosePage()
        {
            Grid baseGrid = Template.FindName("uxBaseRootGrid", this) as Grid;
            TranslateTransform translate = (baseGrid.RenderTransform as TransformGroup).Children[0] as TranslateTransform;
            Storyboard fadeOut = new Storyboard()
            {
                FillBehavior = FillBehavior.HoldEnd
            };
            DoubleAnimation translateAnimation = new DoubleAnimation()
            {
                To = 500,
                Duration = TimeSpan.FromSeconds(0.5)
            };
            DoubleAnimation opacityAnimation = new DoubleAnimation()
            {
                To = 0.0,
                Duration = TimeSpan.FromSeconds(0.4)
            };
            fadeOut.Children.Add(translateAnimation);
            fadeOut.Children.Add(opacityAnimation);
            Storyboard.SetTarget(translateAnimation, baseGrid);
            Storyboard.SetTargetProperty(translateAnimation, new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[0].(TranslateTransform.Y)"));
            Storyboard.SetTarget(opacityAnimation, baseGrid);
            Storyboard.SetTargetProperty(opacityAnimation, new PropertyPath(OpacityProperty));
            fadeOut.Completed += (o, a) =>
            {
                if (Parent != null)
                {
                    ((Panel)Parent).Children.Remove(this);
                }
            };
            fadeOut.Begin();
        }

        protected abstract void Dispose();

        protected object FindItemsPanel(Visual visual)
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(visual); i++)
            {
                Visual child = VisualTreeHelper.GetChild(visual, i) as Visual;
                if (child != null)
                {
                    if (child is object && VisualTreeHelper.GetParent(child) is ItemsPresenter)
                    {
                        object temp = child;
                        return temp;
                    }
                    object panel = FindItemsPanel(child);
                    if (panel != null)
                    {
                        object temp = panel;
                        return temp; // return the panel up the call stack
                    }
                }
            }
            return default(object);
        }
    }
}
