﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Wpf.UI.Core.Controls.Base
{
    public class DraggableImage : Grid
    {
        public delegate void ImageDropped(Image img);
        public event ImageDropped OnImageDropped;

        public delegate void ImageDragStarting();
        public event ImageDragStarting OnImageDragStarting;

        public delegate void ImageDragStarted(Image img);
        public event ImageDragStarted OnImageDragStarted;

        public delegate void ImageDragCompleted();
        public event ImageDragCompleted OnImageDragCompleted;

        private Image iconImage;
        private Image dragableImage = null;
        private Point curPoint = new Point(0, 0);
        private FrameworkElement dropContainer;
        private Grid tmpContainer;
        private bool dragInRealsize = false;
        private bool isDragging = false;
        private double scaleRatio = 1.5;
        private bool isLongPressToDragEnable = false;
        private const int LONG_PRESS_MILLISEC = 500;

        public FrameworkElement DropContainer
        {
            get { return dropContainer; }
            set { dropContainer = value; }
        }

        public Image IconImage
        {
            get { return iconImage; }
            set
            {
                iconImage = value;
                Children.Add(iconImage);
            }
        }

        public Grid TmpContainer { get => tmpContainer; set => tmpContainer = value; }
        public bool IsLongPressToDragEnable { get => isLongPressToDragEnable; set => isLongPressToDragEnable = value; }

        public DraggableImage()
        {
            PreviewTouchDown += DragDropImage_TouchDown;
        }

        private async void DragDropImage_TouchDown(object sender, TouchEventArgs e)
        {
            bool isEnabled = true;
            if (isLongPressToDragEnable)
            {
                int currentWaitMillisec = 0;
                int tick = LONG_PRESS_MILLISEC / 5;
                while (currentWaitMillisec < LONG_PRESS_MILLISEC)
                {
                    await Task.Delay(tick);
                    if (!e.TouchDevice.IsActive)
                    {
                        isEnabled = false;
                        break;
                    }
                    currentWaitMillisec += tick;
                }
            }
            if (isEnabled)
            {
                if (iconImage != null && !isDragging)
                {
                    tmpContainer.Visibility = Visibility.Visible;
                    Point pt = e.GetTouchPoint(tmpContainer).Position;

                    OnImageDragStarting?.Invoke();

                    curPoint = e.GetTouchPoint(this).Position;
                    var targetWidth = iconImage.ActualWidth * scaleRatio;
                    var targetHeight = iconImage.ActualHeight * scaleRatio;
                    dragableImage = new Image();
                    dragableImage.Width = targetWidth;
                    dragableImage.Height = targetHeight;
                    dragableImage.VerticalAlignment = VerticalAlignment.Top;
                    dragableImage.HorizontalAlignment = HorizontalAlignment.Left;
                    dragableImage.RenderTransformOrigin = new Point(0.5, 0.5);
                    dragableImage.RenderTransform = new MatrixTransform(scaleRatio, 0, 0, scaleRatio, pt.X - targetWidth / 2, pt.Y - targetHeight / 2);
                    if (dragInRealsize)
                    {
                        dragableImage.Width = iconImage.Width;
                        dragableImage.Height = iconImage.Height;
                    }
                    dragableImage.Source = iconImage.Source;
                    dragableImage.Opacity = 0.5;
                    dragableImage.TouchMove += dragableImage_TouchMove;
                    dragableImage.TouchLeave += dragableImage_TouchUp;
                    SetZIndex(dragableImage, int.MaxValue);
                    tmpContainer.Children.Add(dragableImage);
                    isDragging = true;
                    OnImageDragStarted?.Invoke(dragableImage);
                    dragableImage.CaptureTouch(e.TouchDevice);
                }
            }
            else
            {
                isDragging = false;
                if (Children.Count > 1)
                {
                    Children.Clear();
                    Children.Add(iconImage);
                }
            }
        }

        private void dragableImage_TouchUp(object sender, TouchEventArgs e)
        {
            isDragging = false;
            dragableImage.TouchMove -= dragableImage_TouchMove;
            dragableImage.TouchLeave -= dragableImage_TouchUp;
            dragableImage.ReleaseAllTouchCaptures();
            OnImageDragCompleted?.Invoke();
            if (dragableImage.Opacity == 1.0)
            {
                Point pt = dragableImage.TranslatePoint(new Point(0, 0), dropContainer);
                if (dropContainer.GetType().Equals(typeof(Grid)))
                {
                    (dropContainer as Grid).Children.Add(new Image()
                    {
                        VerticalAlignment = VerticalAlignment.Top,
                        HorizontalAlignment = HorizontalAlignment.Left,
                        Width = dragableImage.ActualWidth,
                        Height = dragableImage.ActualHeight,
                        Source = dragableImage.Source,
                        RenderTransform = new MatrixTransform(scaleRatio, 0, 0, scaleRatio, pt.X, pt.Y)
                    });
                }
                else
                {
                    OnImageDropped?.Invoke(new Image()
                    {
                        VerticalAlignment = VerticalAlignment.Top,
                        HorizontalAlignment = HorizontalAlignment.Left,
                        Width = dragableImage.ActualWidth,
                        Height = dragableImage.ActualHeight,
                        Source = dragableImage.Source,
                        RenderTransform = new MatrixTransform(scaleRatio, 0, 0, scaleRatio, pt.X, pt.Y)
                    });
                }
            }
            tmpContainer.Children.Clear();
            tmpContainer.Visibility = Visibility.Collapsed;
            if (Children.Count > 1)
            {
                Children.Clear();
                Children.Add(iconImage);
            }
        }

        private void dragableImage_TouchMove(object sender, TouchEventArgs e)
        {
            dragableImage.CaptureTouch(e.TouchDevice);
            Matrix matrix = (dragableImage.RenderTransform as MatrixTransform).Matrix;
            Point pt = e.GetTouchPoint(this).Position;
            matrix.Translate(pt.X - curPoint.X, pt.Y - curPoint.Y);
            dragableImage.RenderTransform = new MatrixTransform(matrix);
            curPoint = pt;
            if (dropContainer != null)
            {
                Point touchPoint = e.GetTouchPoint(dropContainer).Position;
                Rect container = new Rect(dropContainer.RenderSize);
                if (container.Contains(touchPoint))
                {
                    dragableImage.Opacity = 1.0;
                }
                else
                {
                    dragableImage.Opacity = 0.5;
                }
            }
            else
            {
                dragableImage.Opacity = 0.5;
            }
        }
    }
}
