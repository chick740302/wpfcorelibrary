﻿using System.Windows;
using System.Windows.Controls;

namespace Wpf.UI.Core.Controls.Base
{
    public class BaseUserControl : UserControl
    {
        public BaseUserControl()
        {
            Style = new Style(GetType(), FindResource(typeof(BaseUserControl)) as Style);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            //var grid = Template.FindName("uxContentGrid", this);
            //Console.WriteLine(grid);
        }
    }
}
