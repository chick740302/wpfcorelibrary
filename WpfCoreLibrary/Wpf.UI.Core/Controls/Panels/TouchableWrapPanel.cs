﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Wpf.UI.Core.Animations;

namespace Wpf.UI.Core.Controls.Panels
{
    public class TouchableWrapPanel : WrapPanel, INotifyPropertyChanged
    {
        private bool isHeaderBoundryReached, isFooterBoundryReached;
        public event PropertyChangedEventHandler PropertyChanged;
        private bool isPlayStoryboard = false;
        private EasingFunctionBase easingFunc = new CubicEase();
        private FrameworkElement parent;

        public bool IsHeaderBoundryReached
        {
            get { return isHeaderBoundryReached; }
            set
            {
                if (value != isHeaderBoundryReached)
                {
                    isHeaderBoundryReached = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsHeaderBoundryReached"));
                }
            }
        }
        public bool IsFooterBoundryReached
        {
            get { return isFooterBoundryReached; }
            set
            {
                if (value != isFooterBoundryReached)
                {
                    isFooterBoundryReached = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsFooterBoundryReached"));
                }
            }
        }

        private UIElement touchDownElement;
        //public delegate void SelectionChanged(UIElement clickItem);
        //public event SelectionChanged OnSelectionChanged;
        private bool isMove = false;

        public static readonly RoutedEvent SelectionChangedEvent = EventManager.RegisterRoutedEvent("SelectionChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(TouchableWrapPanel));

        public event RoutedEventHandler SelectionChanged
        {
            add { AddHandler(SelectionChangedEvent, value); }
            remove { RemoveHandler(SelectionChangedEvent, value); }
        }

        //public static readonly DependencyProperty OrientationProperty =
        //    DependencyProperty.Register("Orientation", typeof(Orientation),
        //        typeof(TouchableStackPanel), new FrameworkPropertyMetadata(
        //            Orientation.Vertical, FrameworkPropertyMetadataOptions.AffectsMeasure));

        //public Orientation Orientation
        //{
        //    get { return (Orientation)GetValue(OrientationProperty); }
        //    set { SetValue(OrientationProperty, value); }
        //}

        //protected override Size MeasureOverride(Size availableSize)
        //{
        //    Size desiredSize = new Size();

        //    if (Orientation == Orientation.Vertical)
        //        availableSize.Height = Double.PositiveInfinity;
        //    else
        //        availableSize.Width = Double.PositiveInfinity;
        //    foreach (UIElement child in Children)
        //    {
        //        if (child != null)
        //        {
        //            child.Measure(availableSize);

        //            if (Orientation == Orientation.Vertical)
        //            {
        //                desiredSize.Width = Math.Max(desiredSize.Width, child.DesiredSize.Width);
        //                desiredSize.Height += child.DesiredSize.Height;
        //            }
        //            else
        //            {
        //                desiredSize.Height = Math.Max(desiredSize.Height, child.DesiredSize.Height);
        //                desiredSize.Width += child.DesiredSize.Width;
        //            }
        //        }
        //    }
        //    return desiredSize;
        //}

        //protected override Size ArrangeOverride(Size finalSize)
        //{
        //    double offset = 0;
        //    foreach (UIElement child in Children)
        //    {
        //        if (child != null)
        //        {
        //            if (Orientation == Orientation.Vertical)
        //            {
        //                child.Arrange(new Rect(0, offset, finalSize.Width, child.DesiredSize.Height));
        //                offset += child.DesiredSize.Height;
        //            }
        //            else
        //            {
        //                child.Arrange(new Rect(offset, 0, child.DesiredSize.Width, finalSize.Height));
        //                offset += child.DesiredSize.Width;
        //            }
        //        }
        //    }
        //    return finalSize;
        //}

        protected override Geometry GetLayoutClip(Size layoutSlotSize)
        {
            return ClipToBounds ? base.GetLayoutClip(layoutSlotSize) : null;
        }

        public TouchableWrapPanel()
        {
            VirtualizingPanel.SetIsVirtualizing(this, true);
            VirtualizingPanel.SetVirtualizationMode(this, VirtualizationMode.Standard);
            Background = new SolidColorBrush(Color.FromArgb(1, 0, 0, 0));
            FocusVisualStyle = null;
            RenderTransform = new MatrixTransform(1, 0, 0, 1, 0, 0);
            IsManipulationEnabled = true;
            ManipulationStarting += TouchableStackPanel_ManipulationStarting;
            ManipulationDelta += TouchableStackPanel_ManipulationDelta;
            ManipulationInertiaStarting += TouchableStackPanel_ManipulationInertiaStarting;
            ManipulationCompleted += TouchableStackPanel_ManipulationCompleted;
            MouseWheel += TouchableWrapPanel_MouseWheel;
            Loaded += (o, a) =>
            {
                parent = FindParent<FrameworkElement>(this);
            };
            //LayoutUpdated += (o, a) =>
            //{
            //    foreach (UIElement btn in Children)
            //    {
            //        btn.TouchDown += Btn_TouchDown;
            //        btn.MouseDown += Btn_MouseDown;
            //        btn.MouseUp += Btn_MouseUp;
            //    }
            //};
        }

        protected override void OnVisualChildrenChanged(DependencyObject visualAdded, DependencyObject visualRemoved)
        {
            if (visualAdded != null)
            {
                UIElement element = visualAdded as UIElement;
                element.TouchDown += Btn_TouchDown;
                element.MouseDown += Btn_MouseDown;
                element.MouseUp += Btn_MouseUp;
            }
            if (visualRemoved != null)
            {
                UIElement element = visualRemoved as UIElement;
                element.TouchDown -= Btn_TouchDown;
                element.MouseDown -= Btn_MouseDown;
                element.MouseUp -= Btn_MouseUp;
            }
            base.OnVisualChildrenChanged(visualAdded, visualRemoved);
        }

        private void Btn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (!isMove && touchDownElement != null)
            {
                RaiseEvent(new RoutedEventArgs(SelectionChangedEvent, touchDownElement));
            }
            isMove = false;
            touchDownElement = null;
        }

        private void Btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            touchDownElement = sender as UIElement;
        }

        private void TouchableWrapPanel_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (!isPlayStoryboard)
            {
                MatrixTransform xform = RenderTransform as MatrixTransform;
                Matrix rectsMatrix = xform.Matrix;
                if (Orientation == Orientation.Vertical)
                {
                    //IsHeaderBoundryReached = uxScrollViewer.ScrollableWidth == 0 ? true : (rectsMatrix.OffsetX > 0 ? true : false);
                    //IsFooterBoundryReached = uxScrollViewer.ScrollableWidth == 0 ? true : (rectsMatrix.OffsetX < -uxScrollViewer.ScrollableWidth ? true : false);
                    if (rectsMatrix.OffsetX + e.Delta > 0)
                    {
                        rectsMatrix.OffsetX = 0;
                    }
                    else if (rectsMatrix.OffsetX + e.Delta < -ActualWidth + parent.ActualWidth)
                    {
                        rectsMatrix.OffsetX = -ActualWidth + parent.ActualWidth;
                    }
                    else
                    {
                        rectsMatrix.Translate(e.Delta, 0);
                    }
                    IsHeaderBoundryReached = rectsMatrix.OffsetX >= 0 ? true : false;
                    IsFooterBoundryReached = rectsMatrix.OffsetX <= -ActualWidth + parent.ActualWidth ? true : false;
                    RenderTransform = new MatrixTransform(rectsMatrix);
                }
                else
                {
                    //IsHeaderBoundryReached = uxScrollViewer.ScrollableHeight == 0 ? true : (rectsMatrix.OffsetY > 0 ? true : false);
                    //IsFooterBoundryReached = uxScrollViewer.ScrollableHeight == 0 ? true : (rectsMatrix.OffsetY < -uxScrollViewer.ScrollableHeight ? true : false);
                    if (rectsMatrix.OffsetY + e.Delta > 0)
                    {
                        rectsMatrix.OffsetY = 0;
                    }
                    else if (rectsMatrix.OffsetY + e.Delta < -ActualHeight + parent.ActualHeight)
                    {
                        rectsMatrix.OffsetY = -ActualHeight + parent.ActualHeight;
                    }
                    else
                    {
                        rectsMatrix.Translate(0, e.Delta);
                    }
                    IsHeaderBoundryReached = rectsMatrix.OffsetY >= 0 ? true : false;
                    IsFooterBoundryReached = rectsMatrix.OffsetY <= -ActualHeight + parent.ActualHeight ? true : false;
                    RenderTransform = new MatrixTransform(rectsMatrix);
                }
            }
        }

        private void Btn_TouchDown(object sender, TouchEventArgs e)
        {
            touchDownElement = sender as UIElement;
        }

        public static T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);
            if (parentObject == null) return null;
            T parent = parentObject as T;
            if (parent != null)
                return parent;
            else
                return FindParent<T>(parentObject);
        }

        private void TouchableStackPanel_ManipulationCompleted(object sender, System.Windows.Input.ManipulationCompletedEventArgs e)
        {
            if (!isPlayStoryboard)
            {
                if (!isMove && touchDownElement != null)
                {
                    //OnSelectionChanged?.Invoke(touchDownElement);
                    RaiseEvent(new RoutedEventArgs(SelectionChangedEvent, touchDownElement));
                }
                isMove = false;
                touchDownElement = null;

                FrameworkElement rectToMove = e.Source as FrameworkElement;
                MatrixTransform xform = rectToMove.RenderTransform as MatrixTransform;
                Matrix rectsMatrix = xform.Matrix;

                if (Orientation == Orientation.Vertical)
                {
                    if (rectsMatrix.OffsetX > 0)
                    {
                        AligningStoryBoard(this, 0, 0);
                    }
                    else if (rectsMatrix.OffsetX < -ActualWidth + parent.ActualWidth)
                    {
                        AligningStoryBoard(this, -ActualWidth + parent.ActualWidth, 0);
                    }
                }
                else
                {
                    if (rectsMatrix.OffsetY > 0)
                    {
                        AligningStoryBoard(this, 0, 0);
                    }
                    else if (rectsMatrix.OffsetY < -ActualHeight + parent.ActualHeight)
                    {
                        AligningStoryBoard(this, 0, -ActualHeight + parent.ActualHeight);
                    }
                }
            }
        }

        private void TouchableStackPanel_ManipulationInertiaStarting(object sender, ManipulationInertiaStartingEventArgs e)
        {
            //if (!isPlayStoryboard)
            {
                e.TranslationBehavior = new InertiaTranslationBehavior();
                e.TranslationBehavior.InitialVelocity = e.InitialVelocities.LinearVelocity;
                e.TranslationBehavior.DesiredDeceleration = TouchParameters.INERTIA_DECELERATION;
                e.ExpansionBehavior = new InertiaExpansionBehavior();
                e.ExpansionBehavior.InitialVelocity = e.InitialVelocities.ExpansionVelocity;
                e.ExpansionBehavior.DesiredDeceleration = TouchParameters.INERTIA_DECELERATION;
            }
        }

        private void TouchableStackPanel_ManipulationDelta(object sender, System.Windows.Input.ManipulationDeltaEventArgs e)
        {
            if (!isPlayStoryboard)
            {
                FrameworkElement rectToMove = e.Source as FrameworkElement;
                MatrixTransform xform = rectToMove.RenderTransform as MatrixTransform;
                Matrix rectsMatrix = xform.Matrix;
                if (Orientation == Orientation.Vertical)
                {
                    //IsHeaderBoundryReached = uxScrollViewer.ScrollableWidth == 0 ? true : (rectsMatrix.OffsetX > 0 ? true : false);
                    //IsFooterBoundryReached = uxScrollViewer.ScrollableWidth == 0 ? true : (rectsMatrix.OffsetX < -uxScrollViewer.ScrollableWidth ? true : false);
                    if (rectsMatrix.OffsetX > TouchParameters.SCROLL_BOUNDRY_LENGTH || rectsMatrix.OffsetX < -ActualWidth + parent.ActualWidth - TouchParameters.SCROLL_BOUNDRY_LENGTH)
                    {
                        if (e.IsInertial)
                        {
                            e.Complete();
                        }
                        else
                        {
                            rectsMatrix.Translate(e.DeltaManipulation.Translation.X * TouchParameters.MOVE_BOUNDRY_FEEDBACK_RATIO, 0);
                        }
                    }
                    else
                    {
                        rectsMatrix.Translate(e.DeltaManipulation.Translation.X * TouchParameters.MOVE_NORMAL_SCROLL_RATIO, 0);
                    }
                    rectToMove.RenderTransform = new MatrixTransform(rectsMatrix);
                    if (Math.Abs(e.CumulativeManipulation.Translation.X) > TouchParameters.MOVING_THRESHOLD)
                    {
                        isMove = true;
                    }
                }
                else
                {
                    //IsHeaderBoundryReached = uxScrollViewer.ScrollableHeight == 0 ? true : (rectsMatrix.OffsetY > 0 ? true : false);
                    //IsFooterBoundryReached = uxScrollViewer.ScrollableHeight == 0 ? true : (rectsMatrix.OffsetY < -uxScrollViewer.ScrollableHeight ? true : false);
                    if (rectsMatrix.OffsetY > TouchParameters.SCROLL_BOUNDRY_LENGTH || rectsMatrix.OffsetY < -ActualHeight + parent.ActualHeight - TouchParameters.SCROLL_BOUNDRY_LENGTH)
                    {
                        if (e.IsInertial)
                        {
                            e.Complete();
                        }
                        else
                        {
                            rectsMatrix.Translate(0, e.DeltaManipulation.Translation.Y * TouchParameters.MOVE_BOUNDRY_FEEDBACK_RATIO);
                        }
                    }
                    else
                    {
                        rectsMatrix.Translate(0, e.DeltaManipulation.Translation.Y * TouchParameters.MOVE_NORMAL_SCROLL_RATIO);
                    }
                    rectToMove.RenderTransform = new MatrixTransform(rectsMatrix);
                    if (Math.Abs(e.CumulativeManipulation.Translation.Y) > TouchParameters.MOVING_THRESHOLD)
                    {
                        isMove = true;
                    }
                }
            }
        }

        private void TouchableStackPanel_ManipulationStarting(object sender, ManipulationStartingEventArgs e)
        {
            if (!isPlayStoryboard)
            {
                e.ManipulationContainer = parent;
                e.Handled = true;
            }
        }

        private void AligningStoryBoard(FrameworkElement fw, double offsetX, double offsetY, double m11 = 1, double m12 = 0, double m21 = 0, double m22 = 1)
        {
            if (!isPlayStoryboard)
            {
                isPlayStoryboard = true;
                FrameworkElement rectToMove = fw as FrameworkElement;
                Matrix curMatrix = ((MatrixTransform)rectToMove.RenderTransform).Matrix;
                Matrix newMatrix = new Matrix(m11, m12, m21, m22, offsetX, offsetY);
                var animate = new LinearMatrixAnimation(curMatrix, newMatrix, new Duration(TimeSpan.FromSeconds(0.25)));
                animate.EasingFunction = easingFunc;
                animate.FillBehavior = FillBehavior.HoldEnd;

                animate.Completed += (sender, e) =>
                {
                    isPlayStoryboard = false;
                    rectToMove.BeginAnimation(MatrixTransform.MatrixProperty, null); curMatrix = newMatrix;
                    rectToMove.RenderTransform = new MatrixTransform(newMatrix);
                    //IsHeaderBoundryReached = uxScrollViewer.ScrollableWidth == 0 ? true : (newMatrix.OffsetX >= 0 ? true : false);
                    //IsFooterBoundryReached = uxScrollViewer.ScrollableWidth == 0 ? true : (newMatrix.OffsetX <= -uxScrollViewer.ScrollableWidth ? true : false);
                };
                rectToMove.RenderTransform.BeginAnimation(MatrixTransform.MatrixProperty, animate, HandoffBehavior.SnapshotAndReplace);
            }
        }

        public void ScrollToTop(bool isAnimate = true)
        {
            if (!isPlayStoryboard)
            {
                if (isAnimate)
                    AligningStoryBoard(this, 0, 0);
                else
                    RenderTransform = new MatrixTransform(1, 0, 0, 1, 0, 0);
            }
        }

        public void ScrollToBottom()
        {
            if (!isPlayStoryboard)
            {
                AligningStoryBoard(this, -ActualWidth + parent.ActualWidth, 0);
            }
        }
    }
}
