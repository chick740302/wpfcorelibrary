﻿namespace Wpf.UI.Core.Controls.Panels
{
    public class TouchParameters
    {
        public static readonly double MOVING_THRESHOLD = 30d;
        public static readonly double INERTIA_DECELERATION = 0.001d;
        public static readonly double MOVE_BOUNDRY_FEEDBACK_RATIO = 0.02d;
        public static readonly double MOVE_NORMAL_SCROLL_RATIO = 0.85;
        public static readonly int SCROLL_BOUNDRY_LENGTH = 15;
    }
}
