﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Wpf.UI.Core.Animations
{
    public class LinearMatrixAnimation : AnimationTimeline
    {
        #region DependencyProperty

        public Matrix? From
        {
            set { SetValue(FromProperty, value); }
            get { return (Matrix)GetValue(FromProperty); }
        }

        public static DependencyProperty FromProperty = DependencyProperty.Register("From", typeof(Matrix?),
            typeof(LinearMatrixAnimation), new PropertyMetadata(null));

        public IEasingFunction EasingFunction
        {
            get { return (IEasingFunction)GetValue(EasingFunctionProperty); }
            set { SetValue(EasingFunctionProperty, value); }
        }

        public static readonly DependencyProperty EasingFunctionProperty =
            DependencyProperty.Register("EasingFunction", typeof(IEasingFunction), typeof(LinearMatrixAnimation),
                new UIPropertyMetadata(null));

        public Matrix? To
        {
            set { SetValue(ToProperty, value); }
            get { return (Matrix)GetValue(ToProperty); }
        }

        public static DependencyProperty ToProperty = DependencyProperty.Register("To", typeof(Matrix?),
            typeof(LinearMatrixAnimation), new PropertyMetadata(null));

        #endregion

        #region Constructor

        public LinearMatrixAnimation()
        {
            ;
        }

        public LinearMatrixAnimation(Matrix from, Matrix to, Duration duration)
        {
            Duration = duration;
            From = from;
            To = to;
        }

        #endregion

        #region Method Overriding

        public override object GetCurrentValue(object defaultOriginValue, object defaultDestinationValue, AnimationClock animationClock)
        {
            if (animationClock.CurrentProgress == null)
            {
                return null;
            }
            double progress = animationClock.CurrentProgress.Value;

            if (EasingFunction != null)
            {
                progress = EasingFunction.Ease(progress);
            }

            Matrix from = From ?? (Matrix)defaultOriginValue;

            if (To.HasValue)
            {
                Matrix to = To.Value;
                Matrix newMatrix = new Matrix(from.M11 - ((from.M11 - to.M11) * progress),
                                              from.M12 - ((from.M12 - to.M12) * progress),
                                              from.M21 - ((from.M21 - to.M21) * progress),
                                              from.M22 - ((from.M22 - to.M22) * progress),
                                              ((to.OffsetX - from.OffsetX) * progress) + from.OffsetX,
                                              ((to.OffsetY - from.OffsetY) * progress) + from.OffsetY);
                return newMatrix;
            }
            return Matrix.Identity;
        }

        protected override Freezable CreateInstanceCore()
        {
            return new LinearMatrixAnimation();
        }

        public override Type TargetPropertyType
        {
            get { return typeof(Matrix); }
        }

        #endregion
    }
}
