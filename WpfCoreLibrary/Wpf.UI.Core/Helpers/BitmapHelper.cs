﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Wpf.UI.Core.Helpers
{
    public class BitmapHelper
    {
        public static readonly string[] SUPPORT_VID_EXT = { ".mp4", ".mpg", ".mpeg", ".avi", ".wmv" };
        public static readonly string[] SUPPORT_IMG_EXT = { ".jpg", ".jpeg", ".bmp", ".png" };

        private static double DEST_DPIX = 96.0;
        private static double DEST_DPIY = 96.0;

        private readonly static string EMBED_IMG_PATH = "pack://application:,,,/{0};component/{1}";

        public static Size GetImageSize(string path)
        {
            BitmapDecoder decoder = BitmapDecoder.Create(new Uri(@path), BitmapCreateOptions.None, BitmapCacheOption.None);
            return new Size(decoder.Frames[0].Width * (decoder.Frames[0].DpiX / DEST_DPIX), decoder.Frames[0].Height * (decoder.Frames[0].DpiY / DEST_DPIY));
        }

        public static BitmapImage LoadBitmapImage(string path, int width = 0, int height = 0)
        {
            if (File.Exists(path))
            {
                try
                {
                    BitmapImage bitmapImg = new BitmapImage();
                    bitmapImg.BeginInit();
                    bitmapImg.UriSource = new Uri(path, UriKind.RelativeOrAbsolute);
                    bitmapImg.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
                    bitmapImg.CacheOption = BitmapCacheOption.OnLoad;
                    if (width != 0)
                    {
                        bitmapImg.DecodePixelWidth = width;
                    }
                    if (height != 0)
                    {
                        bitmapImg.DecodePixelHeight = height;
                    }
                    bitmapImg.EndInit();
                    bitmapImg.Freeze();
                    return bitmapImg;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static BitmapImage LoadResourceImage(string assemblyName, string dir, int pixelWidth = 0)
        {
            string path = String.Format(EMBED_IMG_PATH, assemblyName, dir);
            BitmapImage bitmapImg = new BitmapImage();
            bitmapImg.BeginInit();
            bitmapImg.UriSource = new Uri(path, UriKind.RelativeOrAbsolute);
            bitmapImg.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
            bitmapImg.CacheOption = BitmapCacheOption.OnLoad;
            if (pixelWidth != 0)
            {
                bitmapImg.DecodePixelWidth = pixelWidth;
            }
            bitmapImg.EndInit();
            bitmapImg.Freeze();
            return bitmapImg;
        }

        public static BitmapImage LoadResourceImage(string filePath, int pixelWidth = 0)
        {
            try
            {
                BitmapImage bitmapImg = new BitmapImage();
                bitmapImg.BeginInit();
                bitmapImg.UriSource = new Uri(filePath, UriKind.RelativeOrAbsolute);
                bitmapImg.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
                bitmapImg.CacheOption = BitmapCacheOption.OnLoad;
                if (pixelWidth != 0)
                {
                    bitmapImg.DecodePixelWidth = pixelWidth;
                }
                bitmapImg.EndInit();
                bitmapImg.Freeze();
                return bitmapImg;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
