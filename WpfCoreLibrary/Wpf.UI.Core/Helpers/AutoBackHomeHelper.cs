﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Wpf.UI.Core.Helpers
{
    public class AutoBackHomeHelper
    {
        private Window target;
        private FrameworkElement popup;
        private DispatcherTimer backHomeTimer = new DispatcherTimer(DispatcherPriority.Background);
        private double backHomeSeconds = 60d;
        private bool IsCanceled { get; set; }

        private bool IsHangEvent = false;

        public Window Target { get => target; set => target = value; }
        public double BackHomeSeconds { get => backHomeSeconds; set => backHomeSeconds = value; }
        public FrameworkElement Popup { get => popup; set => popup = value; }

        public delegate void HomePageEvent();
        public event HomePageEvent OnHomePage;

        public void Start()
        {
            if (target == null)
            {
                throw new Exception("Target window mist be set!");
            }
            IsCanceled = false;
            backHomeTimer.Stop();
            backHomeTimer.Interval = TimeSpan.FromSeconds(backHomeSeconds);
            if (!IsHangEvent)
            {
                backHomeTimer.Tick += BackHomeTimer_Tick;
                target.MouseDown += Target_MouseDown;
                target.MouseMove += Target_MouseMove;
                target.MouseUp += Target_MouseUp;
                target.TouchDown += Target_TouchDown;
                target.TouchMove += Target_TouchMove;
                target.TouchUp += Target_TouchUp;
                IsHangEvent = true;
            }
            backHomeTimer.Start();
        }

        private void Target_TouchUp(object sender, TouchEventArgs e)
        {
            if (IsCanceled)
            {
                return;
            }
            backHomeTimer.Stop();
            if (popup != null)
            {
                if (popup.Visibility != Visibility.Visible)
                {
                    //Todo : check media is completed or not
                    backHomeTimer.Start();
                }
            }
            else
            {
                backHomeTimer.Start();
            }
        }

        private void Target_TouchMove(object sender, TouchEventArgs e)
        {
            if (IsCanceled)
            {
                return;
            }
            backHomeTimer.Stop();
            if (popup != null)
            {
                if (popup.Visibility != Visibility.Visible)
                {
                    //Todo : check media is completed or not
                    backHomeTimer.Start();
                }
            }
            else
            {
                backHomeTimer.Start();
            }
        }

        private void Target_TouchDown(object sender, TouchEventArgs e)
        {
            if (IsCanceled)
            {
                return;
            }
            backHomeTimer.Stop();
            if (popup != null)
            {
                if (popup.Visibility != Visibility.Visible)
                {
                    //Todo : check media is completed or not
                    backHomeTimer.Start();
                }
            }
            else
            {
                backHomeTimer.Start();
            }
        }

        private void Target_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (IsCanceled)
            {
                return;
            }
            backHomeTimer.Stop();
            if (popup != null)
            {
                if (popup.Visibility != Visibility.Visible)
                {
                    //Todo : check media is completed or not
                    backHomeTimer.Start();
                }
            }
            else
            {
                backHomeTimer.Start();
            }
        }

        private void Target_MouseMove(object sender, MouseEventArgs e)
        {
            if (IsCanceled)
            {
                return;
            }
            backHomeTimer.Stop();
            if (popup != null)
            {
                if (popup.Visibility != Visibility.Visible)
                {
                    //Todo : check media is completed or not
                    backHomeTimer.Start();
                }
            }
            else
            {
                backHomeTimer.Start();
            }
        }

        private void Target_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (IsCanceled)
            {
                return;
            }
            backHomeTimer.Stop();
            if (popup != null)
            {
                if (popup.Visibility != Visibility.Visible)
                {
                    //Todo : check media is completed or not
                    backHomeTimer.Start();
                }
            }
            else
            {
                backHomeTimer.Start();
            }
        }

        public void Stop()
        {
            IsCanceled = true;
            target.MouseDown -= Target_MouseDown;
            target.MouseMove -= Target_MouseMove;
            target.MouseUp -= Target_MouseUp;
            target.TouchDown -= Target_TouchDown;
            target.TouchMove -= Target_TouchMove;
            target.TouchUp -= Target_TouchUp;
            backHomeTimer.Tick -= BackHomeTimer_Tick;
            IsHangEvent = false;
            backHomeTimer.Stop();
        }

        public void Reset()
        {
            IsCanceled = false;
            backHomeTimer.Stop();
            backHomeTimer.Start();
        }

        private void BackHomeTimer_Tick(object sender, EventArgs e)
        {
            OnHomePage?.Invoke();
            Reset();
        }
    }
}
