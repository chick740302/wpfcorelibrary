﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Wpf.UI.Core.Converters
{
    public class BooleanReverseConverter : IValueConverter
    {
        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            if (targetType == typeof(bool))
            {
                return !System.Convert.ToBoolean(value, culture);
            }
            throw new InvalidOperationException("Converter can only convert to value of type Visibility.");
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new InvalidOperationException("Converter cannot convert back.");
        }

        public Boolean InvertVisibility { get; set; }
    }
}
