﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Web.HttpServer
{
    public class MicroHttpServer
    {
        private Thread serverThread;
        TcpListener listener;

        public MicroHttpServer(IPAddress address, int port, Func<CompactRequest, CompactResponse> reqProc)
        {
            listener = new TcpListener(address, port);
            serverThread = new Thread(() => {
                listener.Start((int)SocketOptionName.MaxConnections);
                while (true)
                {
                    try
                    {
                        Socket s = listener.AcceptSocket();
                        NetworkStream ns = new NetworkStream(s);
                        StreamReader sr = new StreamReader(ns);
                        CompactRequest req = new CompactRequest(sr);
                        CompactResponse resp = reqProc(req);
                        StreamWriter sw = new StreamWriter(ns);
                        sw.WriteLine("HTTP/1.1 {0}", resp.StatusText);
                        sw.WriteLine("Content-Type: " + resp.ContentType);

                        foreach (string k in resp.Headers.Keys)
                            sw.WriteLine("{0}: {1}", k, resp.Headers[k]);
                        sw.WriteLine("Content-Length: {0}", resp.Data.Length);
                        sw.WriteLine();
                        sw.Flush();

                        s.Send(resp.Data);
                        s.Shutdown(SocketShutdown.Both);
                        ns.Close();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                }
            });
            serverThread.Start();

        }

        public void Stop()
        {
            listener.Stop();
            serverThread.Abort();
        }
    }
}
